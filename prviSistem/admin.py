from django.contrib import admin

from .models import Map, Obejkat

admin.site.register(Map)
admin.site.register(Obejkat)
